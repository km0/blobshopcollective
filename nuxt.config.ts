// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	devtools: { enabled: true },
	css: ["@/assets/css/app.css"],
	postcss: {
		plugins: {
			"postcss-nested": {},
		},
	},
	modules: ["@nuxt/content", "@vueuse/nuxt"],
});
