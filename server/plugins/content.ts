import { visit } from "unist-util-visit";

export default defineNitroPlugin((nitroApp) => {
	nitroApp.hooks.hook("content:file:afterParse", (file) => {
		if (file._id.endsWith(".md")) {
			file.empty = file.body.children.length == 0;
		}
	});
});
