---
title: "unPublic number 81. 'experimental un-publishing'"
description: Session as part of the Experimental Publishing residency at La Générale Nord-Est
where: La Générale Nord-Est, Paris XIV, France
when: 2022-22-06
cover:
links:
    - https://unpublic.bandcamp.com/album/paris-22-june-2022
---

Session as part of the Experimental Publishing residency at La Générale Nord-Est: with Kam Seng Aung (violin), Alexandra Nikolova ([6:] dj mix, voice), Carmen Gray ([1:] drum, bottle, maracas, bell), Chaeyoung Kim ([5:] rattling egg, i-thing, handbell), Emma Prato ([10:] xylophone, harmonic voice), Erica Gargaglione ([2:] (little) drums, bells, flute), Francesco Luzzana ([4:] nanoloop, flute, hands, voice), Jian Haake ([11:] DIY breadboard synth, scratch box), Kimberley Cosmilla ([3:] flute, (little) drum, xylophone), Miriam Schöb ([7:] cracklebox, stylophone, bottle, voice, body), Mitsa Chaida ([0:] maracas, bells, drum, voice), Supisara Burapachaisri ([9:] Bastl Kastle Drum, Bastl Kastle v1.5), Gersande Schellinx ([8:] zither, voice) and Har$ (direction += time-keeping & dictaphones).
