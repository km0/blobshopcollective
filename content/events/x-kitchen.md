---
title: "The X-Kitchen 1.0"
description: Conversations and discussions around raw graduation projects
where: Leeszaal Rotterdam West, Rotterdam
when: 2022-10-17
cover:
links:
---

Mmmm... It’s dinner time! We, the second year students of the Experimental Publishing Master, hereby invite you to our small kitchen to taste our medium-rare concepts! After we've spent a whole day of improvised cooking to get you food for thoughts, we would like to invite you to join us for conversations and discussions around those marinated ideas.

Besides raw graduation projects, soup will be offered. Bon appétit!
