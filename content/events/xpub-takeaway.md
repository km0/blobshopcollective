---
title: "The XPUB-Take-away"
description:
where: Het Boellenpantje, Rotterdam
when: 2022-11-28
cover:
links:
    - https://bollenpandje.nl/
---

Ding! Dong! Your order is ready.

We, the second year students of the Experimental Publishing Master have prepared a small publication to pick-up!

We would like to invite you to join us for a free drink (or two) and little activities around work-in-progress ideas.

Besides the publication, snacks will be offered. See you there!
