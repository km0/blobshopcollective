---
title: "The Breakfast Club #6"
description: Special edition, with tutors and pancakes
where: XPUB Studio, Piet Zwart Institute, Rotterdam
when: 2022-11-12
cover:
links:
    - https://hub.xpub.nl/soupboat/breakfast-club/
---
