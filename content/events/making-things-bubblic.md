---
title: "Making Things Bubblic"
description: Experimental Publishing Graduation Show & Shop 2023
where: Slash Gallery - WORM, Rotterdam
when: 2023-06-30
cover: https://worm.org/wp-content/uploads/2023/06/XPUB23_poster_making_things_bubblic-85f9-768x1024.jpg
links:
    - https://worm.org/production/opening-experimental-publishing-graduation-show-shop-2023-making-things-bubblic-%F0%9F%AB%A7/
---

Graduating students of the Experimental Publishing Master (XPUB), The Piet Zwart Institute, invite you to Making Things Bubblic in WORM’s Slash Gallery, Rotterdam.

A glimpse of a moment. We look at where it comes from, this bubble.

Thinking about bubbles helps to reimagine publishing. When we switch the first letter of public, publishing and publication, something awkward happens that initiates a thought process. A small detail, a single letter, makes unfamiliar and curious again, what we have learned to take for granted.

Making Things Bubblic is, amongst other things, a graduation shop and, a space to continue conversations on how we sell and distribute published matter as independent makers. We are thinking about how to send out our bubbles.

Our bubblishing has happened underwater, throughout the past two years of collective study we have experimented with, and developed our works. This is a bubbling of our experience, an intimate and collective process that now travels up and out to an external public.

-   a bubble is fragile and temporary
-   a bubble is created with care and consciousness
-   or it emerges in a happy accident
-   a bubble is an imaginary, intimate space
-   a bubble pops and spreads without a trace
