---
title: "Special Issue #16 – Learning How to Walk While Catwalking"
description: "Exploring vernacular language processing"
where: Varia, Rotterdam
when: 2021-12-17
cover: https://issue.xpub.nl/img/si16-cover.jpg
links:
    - https://issue.xpub.nl/16/
---

Dear friend and online scroller,  
Beloved internet user,  
Dearest binge watcher and human being IRL,

XPUB1 (The Experimental Publishing Master from Piet Zwart Institute) welcomes you to the Special Issue 16 on vernacular language processing: Learning How to Walk while Catwalking.

Hu? How do you learn how to walk while catwalking? Be confident, be ambitious and be ready to fail a lot.

Our Special Issue is a toolkit to mess around with language: from its standard taxonomies and tags, to its modes of organizing information and its shaping knowledge. With these tools we want to legitimize failures and amatorial practices by proposing a more vernacular understanding of language.

We decided to release the SI16 toolkit in the form of an API (Application Programming Interface). APIs often organise and serve data and knowledge. What is not always evident is that they facilitate the exchange of information between different software programs and systems according to mainly commercial standards and purposes.

Our API is an attempt at a more critical and vernacular approach to such model of distribution. You didn't get a thing yet? Don't worry! We are also on our way and that's the whole point of this experimental enquiry. We will be happy to guide you through the API and the different functions included in it, share our struggles and findings.

Do you want to have a drink and talk with us? We will be outside Varia from 13.00 to 17.00 on Friday December 17th, where there will be a window display, showcasing videos, live-printing and other features of our vernacular toolkit, as well as a drink generator at the door... Wear winter clothes!

Would you rather try-out the toolkit online? No problem! From 13.00 to 17.00, the website of our Special Issue 16 will go on EVENT-MODE: special content will be available ON-THAT-DAY-ONLY giving you more insight on our process. You are welcome to navigate our tools from wherever you are in the Whole Wide World. From 16:00 to 17:00, there will be an online chat where you can ask us questions.
