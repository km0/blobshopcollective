---
title: "Bubble Punch Machine"
description: "Ding! Dong! Your order is ready."
where: Worm, Rotterdam
when: 2023-11-04
cover: https://zinecamp2023.hotglue.me/?program.head.169804918213
links:
    - https://zinecamp2023.hotglue.me/?program
---

Blob Shop collective is bringing the Bubble Punch Machine to Zine Camp in order to serve you free, exciting drinks on Saturday night. Come push the button to activate the machine and get ready for a wild mix and match of ingredients! You will have a fizzy, fun and unique Bubble Punch with a twist.
