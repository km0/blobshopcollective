---
title: "Indiecon 2024"
description: "Blob Shop Collective at Indiecon 2024 (Independent Publishing Festival)"
where: The Gleishalle @Oberhafen, Hamburg, Germany
when: 2024-09-08
cover:
links:
    - https://www.indiecon-festival.com/
---
