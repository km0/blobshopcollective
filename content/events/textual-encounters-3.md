---
title: "Textual Encounters III"
description: " Textual Encounters Workshop Series – Session III: collective editing"
where: Coloniastraat 16, Rotterdam
when: 2024-03-27
cover:
links:
    - https://pad.xpub.nl/p/blobshop_textual_ecounters_session3
---
