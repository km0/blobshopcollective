---
title: "Underground Distribution"
subtitle: "Investigating Subversive Publishing Practices"
description: "Talk at The OEPS! loop"
where: Extrapool, Nijmegen
when: 2024-05-24
cover: https://extrapool.nl/wp-content/uploads/2024/04/Afbeelding1.png
links:
    - https://extrapool.nl/weekend/the-oepsloop/
---

OOPS! We’re looping! 15 years ago Extrapool revealed its iconic red facade after a big renovation of the building. This year we’re circling back and giving it another make-over in collaboration with artists Suzie van Staaveren, Mika Schalks and Blob Shop Collective

During the Extrapool weekend in May we’re kicking this project off, and meeting the artists who will be twisting, bending and curling our building into a new look. Enter the portal for a weekend filled with talks, ideas, workshops, drinks and renewal.

The Blob Shop Collective is a starting collective focused on experimental publishing practices. Jian, Miriam and Emma are the members who will be actively working on this project. Based on their own interest and research into subversive distribution methods with a special interest in the Klek Shops of Sofia, Bulgaria we've asked them to work with the small street-level window of the facade of our building as a potential area of (literal) underground publishing and distribution practices.
