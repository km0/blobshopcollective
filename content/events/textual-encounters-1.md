---
title: "Textual Encounters I"
description: "Textual Encounters Workshop Series – Session I: joint investigation into selected publications"
where: Online
when: 2024-03-21
cover:
links:
    - https://pad.xpub.nl/p/blobshop_textual_ecounters_session1
---
