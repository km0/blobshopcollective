---
title: "Binding Voices Panel Talk, Zine Camp 2023"
description:
where: WORM, Rotterdam
when: 2023-11-04
cover: https://zinecamp2023.hotglue.me/?talks.head.169702041757
links:
    - https://zinecamp2023.hotglue.me/?talks
---

Blob Shop Collective was invited to join the panel talk Binding Voices.

Elisa from Pangaea (a collective based in Den Haag currently working on a publication about collectivity in the field design, architecture and art) and Lu from Not Just a Collective (a publishing practice collective based in Arnhem dedicated to promoting accessibility and community) are excited to host a talk during zine camp 2023.

Elisa and Lu facilitate a conversation between different collectives to discover and reflect on the different shades of what working together means and implies. They aim to explore the notion of "authorship" within collective projects and delve into the advantages and drawbacks of being “officially registered”. If collectivity seems to be the answer, what is the question? How can we work together in this individualistic world?

This interactive discussion, spanning approximately an hour and a half, will unfold through a series of questions and small ‘exercises’ that touch upon various themes. Their objective is to provide a platform where to exchange collective strategies, share poignant stories, and address common challenges, extending an invitation to all participating collectives at zine camp, as well as other like-minded groups or individuals interested in the concept of collectivity, to join them in this enriching discourse.

The talk will be informed by their personal experiences, the experiences of the participating collectives and it will be enriched by the research Pangaea has undertaken over the past year for their forthcoming publication on collectivity. As a meaningful conclusion to the conversation, each collective is invited to conjure a metaphor that best encapsulates their collaborative approach. Together a “cookbook of metaphors” is created for collectives that will also inform Pangaea’s ongoing publication.
