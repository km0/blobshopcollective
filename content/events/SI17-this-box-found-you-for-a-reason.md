---
title: "Special Issue #17 – This Box Found You For a Reason"
description: Page Not Found and the Master Experimental Publishing (XPUB) at the Piet Zwart Institute, Willem de Kooning Academy, invite you to the launch of the Special Issue “This Box Found You For A Reason”
where: Page Not Found, Den Haag
when: 2022-03-25
cover: https://issue.xpub.nl/img/si17-cover.jpg
links:
    - https://issue.xpub.nl/17/
    - https://page-not-found.nl
---

Under the guidance of artist Lídia Pereira and the XPUB staff, the master students explored how features of (video)games are making us more, not less, productive. Life and work are ‘gamified’ through social media, dating apps, and fitness apps designed to increase motivation and productivity. Gamification blurs the lines between play, leisure and labour, to release our collective dopamine for profit. Games in themselves often perform a reproductive role, presenting capitalism as a system of natural laws, exemplified by in-game predatory monetisation schemes. On the other hand, games provide necessary down time and relaxation, helping people function in a largely dysfunctional economy and society. Yet leisure remains a contested space which is still unequally distributed, between genders, ethnicities and abilities. The form of the publication reworks the figure of the loot box, a typically virtual and predatory monetisation scheme.

The launch features presentations by the XPUB students and a discussion with Sepp Eckenhaussen and Koen Bartijn, directors of Platform BK. They will engage in conversation on the economics of the publication, and on larger questions of how the labour of artists and designers is valued and what structures, guidelines and policies can help (small scale) institutions ensure that these practices remain sustainable.

Platform BK researches the role of art in society and takes action for a better art policy. It represents artists, curators, designers, critics and other cultural producers.

Special Issue #17 is co-published by Page Not Found and XPUB, and made by Supisara Burapachaisri, Mitsa Chaida, Kimberley Cosmilla, Erica Gargaglione, Carmen Gray, Jian Haake, Chaeyoung Kim, Francesco Luzzana, Ål Nik (Alexandra Nikolova), Lídia Pereira (ed.), Emma Prato, Gersande Schellinx and Miriam Schöb.
