---
title: "Hello Worlding"
subtitle: "Code documentation as entry point / backdoor to programming practices"
description:
author: Francesco Luzzana, km0, tofu, maya, et all the other aliases
editor:
year: 2023
publisher: Blob Shop Collective
format: 7 x 29,7 cm, bundle of 14 folded chapters
price: 10€
specialPrice:
relatedPubblications:
cover:
links:
    - https://helloworlding.com
    - https://kamomomomomomo.org
---

Hello Worlding is a publication about code documentation, situated in the context of software studies. Branching from `hello world`, a common first exercise when learning a new programming language, it explores code documentation as a publishing surface beyond technical writing.

Coming as a bundle of fourteen short chapters, rich with examples, references, and memes, the publication unfolds two complementary perspectives on documentation.

The first part questions the "nature" of code documentation. While it should ideally serve as an entry point to programming practices, the problematic use of non-inclusive language and technical jargon, combined with a lack of resources for writing, inadvertently creates barriers that gatekeep access. However, there are other ways in.

The second part of the publication introduces the idea of using documentation as a backdoor. The term, borrowed from hacker culture, implies ways to infiltrate software development and to open more doors from the inside. This slight shift of perspective aims to influence the politics of participation and representation while staying close to the source code.

This exploration invites active reading, by browsing through chapters in order, or jumping from one to the other. Like developers in a codebase, readers can forge their own paths through documentation, fostering a world-building process that is never truly finished, echoing the ongoing nature of "Hello Worlding" itself.
