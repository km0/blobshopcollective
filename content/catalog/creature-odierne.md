---
title: Creature Odierne
subtitle: (Nowadays Creatures)
description:
author: Francesco Luzzana, camo, gafas, peru, et all
editor:
year: 2023
publisher: Blob Shop Collective
format: A6 zine, 32 pp
price: 8€
specialPrice:
relatedPubblications:
    - Familiar Faces
cover:
links:
    - https://kamomomomomomo.org
---

A non-comprehensive catalog of contemporary critters, current creatures, and modern mythologies coming from the most marginal places of the earth, seas, lakes, rivers, atolls, cities, villages with less than one hundred inhabitants, from the most remote dreams, etc. At the same time scientifically accurate and terribly misleading, this compendium of 60 little pests (2 omitted because NDA) is meant to provide confort to these people that like animals, but suffer thinking about them locked into cages at the zoo. Or to the ones that like bad ideas, but are too cautiuous to put them in practice. Or to these medieval people that back in the days where making a living by drawing books full of crazy creatures. You are my public and I entrust you this volume, use it properly.
