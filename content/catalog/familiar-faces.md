---
title: Familiar Faces
subtitle:
description:
author: Francesco Luzzana, kamut, tufo, peru, et all
editor:
year: 2024
publisher: Blob Shop Collective
format: A6 zine, 30 pp
price: 8€
specialPrice:
relatedPubblications:
    - Creature Odierne
cover:
links:
    - https://kamomomomomomo.org
---

This collection of familiar faces is the by-product of the ongoing research on emotional intelligence and felt feelings called life. Sometimes it is very hard to put your finger on a sensation and give it a name, so I decided to keep a face-based journal. Happiness, anger, hunger, wonder are nothing but empty terms to fill our faces with. What's really necessary has to be found in the interstice between a smile and a grin, between cold sweat and goosebump, between twenty pairs of eyes and a clover of nostrils. As someone once wrote behind this very publication: most people I know have a face, so I think these drawing are really relatable. 😀😂😉😋😘🥲😎🫥😶🙄😫🥱😴 🤒🫨🫣🤫😭😨🥵😱😕🙃🫤😒🫠 🫥🤔😐🤨☺️🫡
